﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TurnosWeb.Models.Enum;
using TurnosWeb.Models.MetaData;

namespace TurnosWeb.Models
{
    [Table("Horario")]
    [MetadataType(typeof(IHorario))]
    public class Horario:EntidadBase,IHorario
    {

        public Guid MedicoId { get; set; }

        
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFin { get; set; }

        //Enums        
        public DiaDeLaSemana Dia { get; set; }

        //Propiedades de Navegacion
        [ForeignKey("MedicoId")]
        public virtual Medico Medico { get; set; }
    }
}