﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnosWeb.Models.MetaData
{
    public interface IObraSocial
    {
        [Required(AllowEmptyStrings =false, ErrorMessage ="La {0} es obligatoria")]
        [DataType(DataType.Text)]
         string Descripcion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El {0} es obligatorio")]
        [DataType(DataType.Text)]
        string Codigo { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La {0} es obligatoria")]
        [DataType(DataType.Text)]
        [StringLength(5,MinimumLength =2,ErrorMessage ="La {0} tiene que tener entre {2} y {1} caracteres")]
        string Abreviacion { get; set; }
    }
}
