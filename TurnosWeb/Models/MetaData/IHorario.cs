﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models.Enum;

namespace TurnosWeb.Models.MetaData
{
    public interface IHorario
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El {0} es un Campo Obligatorio")]
        TimeSpan HoraInicio { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "El {0} es un Campo Obligatorio")]
        TimeSpan HoraFin { get; set; }

        [EnumDataType(typeof(DiaDeLaSemana))]
        DiaDeLaSemana Dia { get; set; }
    }
}
