﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnosWeb.Models.MetaData
{
    public interface IEspecialidad
    {

        [Required(AllowEmptyStrings = false, ErrorMessage = "El {0} es un campo obligatorio")]
        [Display(Name = "Nombre")]
        [StringLength(80, ErrorMessage = "el {0} debe ser menor a {1} caracteres")]
        string Descripcion { get; set; }
    }

}
