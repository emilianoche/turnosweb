﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models.Enum;

namespace TurnosWeb.Models.MetaData
{
    public interface IPaciente
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Campo Obligatorio")]
        [StringLength(250, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string  Nombre { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Campo Obligatorio")]
        [StringLength(250, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string Apellido { get; set; }
        [StringLength(25, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string Telefono { get; set; }
        [StringLength(250, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string Celular { get; set; }
        [StringLength(250, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        [DataType(DataType.EmailAddress)]
        string Email { get; set; }
        [StringLength(400, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.")]
        string Direccion { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Campo Obligatorio")]
        [StringLength(8, ErrorMessage = "El campo {0} no debe superar los {1} caracteres.", MinimumLength = 7)]
        string Dni { get; set; }
        [Required(ErrorMessage = "Campo Obligatorio")]
        [DataType(DataType.Date)]
        [Display(Name ="@Fecha de Nacimiento")]
        DateTime FechaNacimiento { get; set; }
        bool EstaBorrado { get; set; }

        [EnumDataType(typeof(EstadoCivil))]
        EstadoCivil EstadoCivil { get; set; }
        [EnumDataType(typeof(Sexo))]
        Sexo Sexo { get; set; }
        [EnumDataType(typeof(GrupoSanguineo))]
        GrupoSanguineo GrupoSanguineo { get; set; }
    }
}
