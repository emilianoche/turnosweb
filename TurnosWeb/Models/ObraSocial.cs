﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TurnosWeb.Models.MetaData;

namespace TurnosWeb.Models
{
    [Table("ObraSocial")]
    [MetadataType(typeof(IObraSocial))]
    public class ObraSocial:EntidadBase,IObraSocial
    {
        public string Descripcion { get; set; }
        public string Codigo { get; set; }
        public string Abreviacion { get; set; }

        public virtual ICollection<Paciente> Pacientes { get; set; }
        public virtual ICollection<Turno> Turno { get; set; }
    }
}