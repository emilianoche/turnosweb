﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models.Enum;
using TurnosWeb.Models.MetaData;

namespace TurnosWeb.Models
{
    [Table("Turno")]
    [MetadataType(typeof(ITurno))]
    public class Turno:EntidadBase,ITurno
    {
        // Ids
        public Guid MedicoId { get; set; }
        public Guid PacienteId { get; set; }
        public Guid? ObraSocialId { get; set; }

        // Propiedades Core
        public DateTime Fecha { get; set; }
        public TimeSpan HoraTurno { get; set; }
        public string MotivoConsulta { get; set; }
        public bool EsUrgente { get; set; }
        public bool EsPrimeraVez { get; set; }
        public decimal Monto { get; set; }


        // Enums
        public EstadoTurno EstadoTurno { get; set; }

        // Propiedades de Navegacion
        [ForeignKey("MedicoId")]
        public virtual Medico Medico { get; set; }
        [ForeignKey("PacienteId")]
        public virtual Paciente Paciente { get; set; }
        [ForeignKey("ObraSocialId")]
        public virtual ObraSocial ObraSocial { get; set; }
    }
}
