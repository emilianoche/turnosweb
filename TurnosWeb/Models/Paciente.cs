﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TurnosWeb.Models.Enum;
using TurnosWeb.Models.MetaData;

namespace TurnosWeb.Models
{
    [Table("Paciente")]
    [MetadataType(typeof(IPaciente))]
    public class Paciente : EntidadBase, IPaciente
    {
        public Guid  UsuarioId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Direccion { get; set; }
        public string Dni { get; set; }
        public string Email { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public bool EstaBorrado { get; set; }


        [EnumDataType(typeof(EstadoCivil))]
        public EstadoCivil EstadoCivil { get; set; }
        [EnumDataType(typeof(GrupoSanguineo))]
        public GrupoSanguineo GrupoSanguineo { get; set; }
        [EnumDataType(typeof(Sexo))]
        public Sexo Sexo { get; set; }
        

       
        //[InverseProperty("Email")]
        [ForeignKey("Email")]
        public virtual ApplicationUser Usuario {get;set;}
        public virtual ICollection<ObraSocial> ObrasSociales { get; set; }
        public virtual ICollection<Turno> Turnos { get; set; }

    }
}