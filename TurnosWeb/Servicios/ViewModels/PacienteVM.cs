﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models.Enum;

namespace TurnosWeb.Servicios.ViewModels
{
    public class PacienteVM
    {
        public Guid Id { get; set; }
       
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Campo Obligatorio", AllowEmptyStrings = false)]
        [StringLength(150, ErrorMessage = "El campo {0} no puede ser mayor a {1} caracter")]
        public string Nombre { get; set; }
        [Display(Name = "Apellido")]
        [StringLength(150, ErrorMessage = "El campo {0} no puede ser mayor a {1} caracter")]
        [Required(ErrorMessage = "Campo Obligatorio", AllowEmptyStrings = false)]
        public string Apellido { get; set; }
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }
        [Display(Name = "Celular")]
        public string Celular { get; set; }
        [Required(ErrorMessage = "Campo Obligatorio", AllowEmptyStrings = false)]
        [Display(Name = "Documento")]
        public string DNI { get; set; }
        [Required(ErrorMessage = "Campo Obligatorio", AllowEmptyStrings = false)]
        [Display(Name = "E-Mail")]
        [DataType(DataType.EmailAddress)]
        public string Mail { get; set; }
        [Required(ErrorMessage = "Campo Obligatorio", AllowEmptyStrings = false)]
        [Display(Name ="Direccion")]
        public string Direccion { get; set; }
        [Required(ErrorMessage = "Campo Obligatorio", AllowEmptyStrings = false)]
        [Display(Name ="Fecha de Nacimiento")]
        [DataType(DataType.Date)]
        public DateTime FechaNacimiento { get; set; }

        public EstadoCivil EstadoCivil { get; set; }
        public GrupoSanguineo GrupoSanguineo { get; set; }
        public Sexo Sexo { get; set; }


    }
}
